// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/18 17:34
// 文件名称 ：   user_service.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package service

import (
	"gitee.com/JohnsonSmile/microshop/user/domain/model"
	"gitee.com/JohnsonSmile/microshop/user/domain/repository"
	"gitee.com/JohnsonSmile/microshop/user/common"
)

type IUserDataService interface {
	// 添加用户
	AddUser(user *model.User) (uid int64, err error)
	// 删除用户
	DeleteUser(uid int64) (err error)
	// 更新用户
	UpdateUser(user *model.User, isChangePwd bool) (err error)
	// 查找用户
	FindUserByName(username string) (user *model.User, err error)
	// 校验用户密码
	CheckPwd(username string, pwd string) (err error)
}

func NewUserDataService(rep repository.IUserRepository) IUserDataService {
	return &UserDataService{rep: rep}
}

type UserDataService struct {
	rep repository.IUserRepository
}

// 插入用户
func (u *UserDataService) AddUser(user *model.User) (uid int64, err error) {
	// 对用户密码加密
	password, err := common.GeneratePassword(user.HashPassword)
	if err != nil {
		return 0, err
	}
	user.HashPassword = string(password)
	return u.rep.CreateUser(user)
}

// 删除用户
func (u *UserDataService) DeleteUser(uid int64) (err error) {
	return u.rep.DeleteUserByID(uid)
}

// 更新用户
func (u *UserDataService) UpdateUser(user *model.User, isChangePwd bool) (err error) {
	if isChangePwd {
		password, err := common.GeneratePassword(user.HashPassword)
		if err != nil {
			return err
		}
		user.HashPassword = string(password)
	}
	// 其他逻辑,比如写日志,消息队列等等,或者在这个里面写redis缓存.
	return u.rep.UpdateUser(user)
}

// 根据用户名查找用户信息
func (u *UserDataService) FindUserByName(username string) (user *model.User, err error) {
	return u.rep.FindUserByName(username)
}

func (u *UserDataService) CheckPwd(username string, pwd string) (err error) {
	user, err := u.rep.FindUserByName(username)
	if err != nil {
		return err
	}
	return common.ValidatePassword(pwd, user.HashPassword)
}
