// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/18 17:10
// 文件名称 ：   user_repository.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package repository

import (
	"gitee.com/JohnsonSmile/microshop/user/domain/model"
	"github.com/jinzhu/gorm"
	_ "github.com/go-sql-driver/mysql"
)

type IUserRepository interface {
	// 初始化表
	InitTable() (err error)
	// 根据用户名查找用户信息
	FindUserByName(username string) (user *model.User, err error)
	// 根据用户id查找用户信息
	FindUserByID(uid int64) (user *model.User, err error)
	// 创建用户
	CreateUser(user *model.User) (uid int64, err error)
	// 根据id删除用户
	DeleteUserByID(uid int64) (err error)
	// 更新用户信息
	UpdateUser(user *model.User) (err error)
	// 查找所有
	FindAll() (users []*model.User, err error)
}

// 创建UserRepository
func NewUserRepository(db *gorm.DB) IUserRepository {
	return &UserRepository{db: db}
}

type UserRepository struct {
	db *gorm.DB
}

// 创建表
func (u *UserRepository) InitTable() (err error) {
	return u.db.CreateTable(&model.User{}).Error
}

// 根据用户名查找用户信息
func (u *UserRepository) FindUserByName(username string) (user *model.User, err error) {
	user = &model.User{}
	return user, u.db.Where("user_name = ?", username).Find(user).Error
}

// 根据用户ID查找用户信息
func (u *UserRepository) FindUserByID(uid int64) (user *model.User, err error) {
	user = &model.User{}
	return user, u.db.First(user, uid).Error
}

// 创建用户
func (u *UserRepository) CreateUser(user *model.User) (uid int64, err error) {
	return user.ID, u.db.Create(user).Error
}

// 根据id删除用户
func (u *UserRepository) DeleteUserByID(uid int64) (err error) {
	return u.db.Where("id = ?", uid).Delete(&model.User{}).Error
}

// 更新用户信息
func (u *UserRepository) UpdateUser(user *model.User) (err error) {
	return u.db.Model(user).Update(user).Error
}

// 查找所有
func (u *UserRepository) FindAll() (users []*model.User, err error) {
	return users, u.db.Find(users).Error
}
