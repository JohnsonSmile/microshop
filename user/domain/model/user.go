// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/18 17:07
// 文件名称 ：   user_repository.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package model

type User struct {
	// 主键
	ID int64 `gorm:"primary_key;not_null;auto_increment"`
	// 用户名称
	UserName string `gorm:"unique_index;not_null"`
	// 添加需要的字段
	FirstName string
	// ...
	// 密码
	HashPassword string
}
