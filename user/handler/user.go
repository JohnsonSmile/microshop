package handler

import (
	"context"
	"gitee.com/JohnsonSmile/microshop/user/domain/model"
	"gitee.com/JohnsonSmile/microshop/user/domain/service"
	"gitee.com/JohnsonSmile/microshop/user/proto"
)

type UserHandler struct {
	UserDataService service.IUserDataService
}

func (u *UserHandler) Register(ctx context.Context, request *user.UserRegisterRequest, response *user.UserRegisterResponse) error {
	userRegister := &model.User{
		UserName:     request.UserName,
		FirstName:    request.FirstName,
		HashPassword: request.Pwd,
	}
	_, err := u.UserDataService.AddUser(userRegister)
	if err != nil {
		response.Message = "用户注册失败!"
		return err
	}
	response.Message = "用户注册成功!"
	return nil
}

func (u *UserHandler) Login(ctx context.Context, request *user.UserLoginRequest, response *user.UserLoginResponse) error {
	err := u.UserDataService.CheckPwd(request.UserName, request.Pwd)
	if err != nil {
		response.IsSuccess = false
		return err
	}
	response.IsSuccess = true
	return nil
}

func (u *UserHandler) GetUserInfo(ctx context.Context, request *user.UserInfoRequest, response *user.UserInfoResponse) error {
	userInfo, err := u.UserDataService.FindUserByName(request.UserName)
	if err != nil {
		return err
	}
	response.UserName = userInfo.UserName
	response.FirstName = userInfo.FirstName
	response.UserId = userInfo.ID
	return nil
}
