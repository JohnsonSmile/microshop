// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/18 17:51
// 文件名称 ：   util.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package common

import "golang.org/x/crypto/bcrypt"

// 加密用户密码
func GeneratePassword(password string) (hashed []byte, err error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

// 验证用户密码
func ValidatePassword(password string, hashed string) (err error) {
	if err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(password)); err != nil {
		return err
	}
	return nil
}
