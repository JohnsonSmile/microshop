// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/20 16:25
// 文件名称 ：   product_seo.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package model

type ProductSeo struct {
	ID             int64  `gorm:"primary_key;not_null;auto_increment" json:"id"`
	SeoTitle       string `json:"seo_title"`
	SeoKeywords    string `json:"seo_keywords"`
	SeoDescription string `json:"seo_description"`
	SeoCode        string `gorm:"unique_index;not_null" json:"seo_code"`
	SeoProductID   int64  `json:"seo_product_id"`
}
