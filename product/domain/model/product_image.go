// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/20 16:24
// 文件名称 ：   product_image.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package model

type ProductImage struct {
	ID             int64  `gorm:"primary_key;not_null;auto_increment" json:"id"`
	ImageName      string `json:"image_name"`
	ImageCode      string `gorm:"unique_index;not_null" json:"image_code"`
	ImageUrl       string `json:"image_url"`
	ImageProductID int64  `json:"image_product_id"`
}
