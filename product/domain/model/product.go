// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/20 16:19
// 文件名称 ：   product.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package model

type Product struct {
	ID                 int64          `gorm:"primary_key;not_null;auto_increment"`
	ProductName        string         `json:"product_name"`
	ProductSku         string         `gorm:"unique_index;not_null" json:"product_sku"`
	ProductPrice       float64        `json:"product_price"`
	ProductDescription string         `json:"product_description"`
	ProductImage       []ProductImage `gorm:"ForeignKey:ImageProductID" json:"product_image"`
	ProductSize        []ProductSize  `gorm:"ForeignKey:SizeProductID" json:"product_size"`
	ProductSeo         ProductSeo     `gorm:"ForeignKey:SeoProductID" json:"product_seo"`
}
