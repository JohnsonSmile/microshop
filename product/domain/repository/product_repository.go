// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/20 16:33
// 文件名称 ：   product_repository.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package repository

import (
	"github.com/jinzhu/gorm"
	"product/domain/model"
)

type IProductRepository interface {
	InitTable() error
	FindProductByID(id int64) (product *model.Product, err error)
	CreateProduct(product *model.Product) (id int64, err error)
	DeleteProductByID(id int64) (err error)
	UpdateProduct(product *model.Product) (err error)
	FindAllProducts() (products []model.Product, err error)
}

// 创建ProductRepository
func NewProductRepository(db *gorm.DB) IProductRepository {
	return &ProductRepository{db: db}
}

type ProductRepository struct {
	db *gorm.DB
}

// 初始化表
func (p *ProductRepository) InitTable() error {
	return p.db.CreateTable(&model.Product{}, &model.ProductImage{}, &model.ProductSeo{}, &model.ProductSize{}).Error
}

// 根据id查找商品
func (p *ProductRepository) FindProductByID(id int64) (product *model.Product, err error) {
	product = &model.Product{}
	return product, p.db.Preload("ProductImage").Preload("ProductSize").Preload("ProductSeo").First(product, id).Error
}

// 创建商品
func (p *ProductRepository) CreateProduct(product *model.Product) (id int64, err error) {
	return product.ID, p.db.Create(product).Error
}

// 根据id删除商品
func (p *ProductRepository) DeleteProductByID(id int64) (err error) {
	// 开启事务
	tx := p.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if tx.Error != nil {
		return tx.Error
	}
	// 删除
	if err = tx.Unscoped().Where("id = ?", id).Delete(&model.Product{}).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err = tx.Unscoped().Where("image_product_id = ?", id).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err = tx.Unscoped().Where("size_product_id = ?", id).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err = tx.Unscoped().Where("seo_product_id = ?", id).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error

}

// 更新Product信息
func (p *ProductRepository) UpdateProduct(product *model.Product) (err error) {
	return p.db.Model(product).Update(product).Error
}

// 获取所有的商品信息
func (p *ProductRepository) FindAllProducts() (products []model.Product, err error) {
	return products, p.db.Preload("ProductImage").Preload("ProductSize").Preload("ProductSeo").Find(&products).Error
}
