// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/20 16:53
// 文件名称 ：   product_service.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package service

import (
	"product/domain/model"
	"product/domain/repository"
)

type IProductDataService interface {
	AddProduct(product *model.Product) (id int64, err error)
	DeleteProduct(id int64) (err error)
	UpdateProduct(product *model.Product) (err error)
	FindProductByID(id int64) (product *model.Product, err error)
	FindAllProducts() (products []model.Product, err error)
}

// 创建dataservice
func NewProductDataService(rep repository.IProductRepository) IProductDataService {
	return &ProductDataService{rep: rep}
}

type ProductDataService struct {
	rep repository.IProductRepository
}

func (p *ProductDataService) AddProduct(product *model.Product) (id int64, err error) {
	return p.rep.CreateProduct(product)
}

func (p *ProductDataService) DeleteProduct(id int64) (err error) {
	return p.rep.DeleteProductByID(id)
}

func (p *ProductDataService) UpdateProduct(product *model.Product) (err error) {
	return p.rep.UpdateProduct(product)
}

func (p *ProductDataService) FindProductByID(id int64) (product *model.Product, err error) {
	return p.rep.FindProductByID(id)
}

func (p *ProductDataService) FindAllProducts() (products []model.Product, err error) {
	return p.rep.FindAllProducts()
}
