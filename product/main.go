package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-plugins/registry/consul/v2"
	ratelimit "github.com/micro/go-plugins/wrapper/ratelimiter/uber/v2"
	opentracing2 "github.com/micro/go-plugins/wrapper/trace/opentracing/v2"
	"github.com/opentracing/opentracing-go"
	"product/common"
	"product/domain/repository"
	"product/domain/service"
	"product/handler"
	"product/proto"
)

var QPS = 100;

func main() {

	// 配置中心
	conf, err := common.GetConsulConfig("47.112.13.65", 8500, "/micro/config")
	if err != nil {
		logger.Error(err)
	}
	// 注册中心
	reg := consul.NewRegistry(
		registry.Addrs("47.112.13.65:8500"),
	)

	// 链路追踪
	tracer, closer, err := common.NewTracer("go.micro.service.product", "47.112.13.65:6831")
	if err != nil {
		logger.Error(err)
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	// 创建服务
	srv := micro.NewService(
		micro.Name("go.micro.service.product"),
		micro.Version("latest"),
		micro.Registry(reg),
		micro.WrapHandler(opentracing2.NewHandlerWrapper(opentracing.GlobalTracer())),
		micro.WrapHandler(ratelimit.NewHandlerWrapper(QPS)),
	)
	srv.Init()

	// 创建数据库
	mysqlConf, err := common.GetMysqlConfigFromConsul(conf, "mysql")
	if err != nil {
		logger.Fatal(err)
	}
	mysqlArgs := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=true&loc=Local",
		mysqlConf.User,
		mysqlConf.Pwd,
		mysqlConf.Host,
		mysqlConf.Port,
		mysqlConf.Database,
	)
	db, err := gorm.Open("mysql", mysqlArgs)
	if err != nil {
		logger.Error(err)
	}
	defer db.Close()
	db.SingularTable(true)

	// 创建repository
	repo := repository.NewProductRepository(db)
	err = repo.InitTable()
	if err != nil {
		logger.Error(err)
	}

	// 创建dataservice
	dataService := service.NewProductDataService(repo)

	// 注册handler
	err = product.RegisterProductHandler(srv.Server(), &handler.ProductHandler{DataService: dataService})
	if err != nil {
		logger.Error(err)
	}

	// 开启服务
	if err = srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
