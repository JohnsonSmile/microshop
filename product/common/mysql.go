// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/19 11:24
// 文件名称 ：   mysql.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package common

import "github.com/micro/go-micro/v2/config"

type MysqlConfig struct {
	Host     string `json:"host"`
	User     string `json:"user"`
	Pwd      string `json:"pwd"`
	Database string `json:"database"`
	Port     int64  `json:"port"`
}

// 获取mysql配置
func GetMysqlConfigFromConsul(conf config.Config, path ...string) (mysqlConf *MysqlConfig, err error) {
	mysqlConf = &MysqlConfig{}
	err = conf.Get(path...).Scan(mysqlConf)
	if err != nil {
		return nil, err
	}
	return mysqlConf, nil
}
