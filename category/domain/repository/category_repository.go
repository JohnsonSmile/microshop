// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/19 01:02
// 文件名称 ：   category_repository.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package repository

import (
	"gitee.com/JohnsonSmile/microshop/category/domain/model"
	"github.com/jinzhu/gorm"
)

type ICategoryRepository interface {
	// 初始化表
	InitTable() (err error)
	// 创建category
	CreateCategory(category *model.Category) (cid int64, err error)
	// 根据id删除category
	DeleteCategoryByID(cid int64) (err error)
	// 更新category
	UpdateCategory(category *model.Category) (err error)
	// 查询所有的category
	FindAll() (categories []*model.Category, err error)
	// 根据id查询category
	FindCategoryByID(cid int64) (category *model.Category, err error)
	// 根据名称查询category
	FindCategoryByName(name string) (category *model.Category, err error)
	// 根据level查询
	FindCategoryByLevel(level uint32) (categories []*model.Category, err error)
	// 根据parentId查询
	FindCategoryByParent(pid int64) (categories []*model.Category, err error)
}

// 创建CategoryRepository
func NewCategoryRepository(db *gorm.DB) ICategoryRepository {
	return &CategoryRepository{db: db}
}

type CategoryRepository struct {
	db *gorm.DB
}

// 创建表
func (c *CategoryRepository) InitTable() (err error) {
	return c.db.CreateTable(&model.Category{}).Error
}

// 根据id查询分类信息
func (c *CategoryRepository) FindCategoryByID(cid int64) (category *model.Category, err error) {
	category = &model.Category{}
	return category, c.db.First(category, cid).Error
}

// 新建分类
func (c *CategoryRepository) CreateCategory(category *model.Category) (cid int64, err error) {
	return category.ID, c.db.Create(category).Error
}

// 删除分类
func (c *CategoryRepository) DeleteCategoryByID(cid int64) (err error) {
	return c.db.Where("id = ?").Delete(&model.Category{}).Error
}

// 更新分类
func (c *CategoryRepository) UpdateCategory(category *model.Category) (err error) {
	return c.db.Model(category).Update(category).Error
}

// 查询所有的信息
func (c *CategoryRepository) FindAll() (categories []*model.Category, err error) {
	return categories, c.db.Find(categories).Error
}

// 根据名称查询category
func (c *CategoryRepository) FindCategoryByName(name string) (category *model.Category, err error) {
	category = &model.Category{}
	return category, c.db.Where("category_name = ?", name).Find(category).Error
}

// 根据level查询
func (c *CategoryRepository) FindCategoryByLevel(level uint32) (categories []*model.Category, err error) {
	return categories, c.db.Where("category_level = ?", level).Find(categories).Error
}

// 根据parentId查询
func (c *CategoryRepository) FindCategoryByParent(pid int64) (categories []*model.Category, err error) {
	return categories, c.db.Where("category_parent = ?", pid).Find(categories).Error
}
