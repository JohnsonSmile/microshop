// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/19 01:15
// 文件名称 ：   category_service.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package service

import (
	"gitee.com/JohnsonSmile/microshop/category/domain/model"
	"gitee.com/JohnsonSmile/microshop/category/domain/repository"
)

type ICategoryDataService interface {
	AddCategory(category *model.Category) (cid int64, err error)
	DeleteCategory(cid int64) (err error)
	UpdateCategory(category *model.Category) (err error)
	FindCategoryByID(cid int64) (category *model.Category, err error)
	FindAllCategories() (categories []*model.Category, err error)
	// 根据名称查询category
	FindCategoryByName(name string) (category *model.Category, err error)
	// 根据level查询
	FindCategoryByLevel(level uint32) (categories []*model.Category, err error)
	// 根据parentId查询
	FindCategoryByParent(pid int64) (categories []*model.Category, err error)
}

// 创建分类的数据服务
func NewCategoryDataService(categoryRepository repository.ICategoryRepository) ICategoryDataService {
	return &CategoryDataService{repo: categoryRepository}
}

type CategoryDataService struct {
	repo repository.ICategoryRepository
}

func (c *CategoryDataService) AddCategory(category *model.Category) (cid int64, err error) {
	return c.repo.CreateCategory(category)
}

func (c *CategoryDataService) DeleteCategory(cid int64) (err error) {
	return c.repo.DeleteCategoryByID(cid)
}

func (c *CategoryDataService) UpdateCategory(category *model.Category) (err error) {
	return c.repo.UpdateCategory(category)
}

func (c *CategoryDataService) FindCategoryByID(cid int64) (category *model.Category, err error) {
	return c.repo.FindCategoryByID(cid)
}

func (c *CategoryDataService) FindAllCategories() (categories []*model.Category, err error) {
	return c.FindAllCategories()
}

func (c *CategoryDataService) FindCategoryByName(name string) (category *model.Category, err error) {
	return c.repo.FindCategoryByName(name)
}

func (c *CategoryDataService) FindCategoryByLevel(level uint32) (categories []*model.Category, err error) {
	return c.repo.FindCategoryByLevel(level)
}

func (c *CategoryDataService) FindCategoryByParent(pid int64) (categories []*model.Category, err error) {
	return c.repo.FindCategoryByParent(pid)
}
