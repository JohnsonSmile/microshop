// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/19 01:01
// 文件名称 ：   category.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package model

type Category struct {
	ID                  int64  `gorm:"primary_key;not_null;auto_increment" json:"id"`
	CategoryName        string `gorm:"unique_index;not_null" json:"category_name"`
	CategoryLevel       uint32 `json:"category_level"`
	CategoryParent      int64  `json:"category_parent"`
	CategoryImage       string `json:"category_image"`
	CategoryDescription string `json:"category_description"`
}
