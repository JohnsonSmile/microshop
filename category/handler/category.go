package handler

import (
	"context"
	"gitee.com/JohnsonSmile/microshop/category/domain/model"
	"gitee.com/JohnsonSmile/microshop/category/domain/service"
	"gitee.com/JohnsonSmile/microshop/category/proto"
	"gitee.com/JohnsonSmile/microshop/category/common"
)

type CategoryHandler struct {
	CategoryService service.ICategoryDataService
}

// 提供创建分类的服务
func (c *CategoryHandler) CreateCategory(ctx context.Context, request *category.CategoryRequest, response *category.CreateCategoryResponse) error {
	cate := &model.Category{
		CategoryName:        request.CategoryName,
		CategoryLevel:       request.CategoryLevel,
		CategoryParent:      request.CategoryParent,
		CategoryImage:       request.CategoryImage,
		CategoryDescription: request.CategoryDescription,
	}
	cid, err := c.CategoryService.AddCategory(cate)
	if err != nil {
		response.CategoryId = cid
		response.Message = "添加分类失败!"
		return err
	}
	response.CategoryId = cid
	response.Message = "添加分类成功!"
	return nil
}

// 更新分类
func (c *CategoryHandler) UpdateCategory(ctx context.Context, request *category.CategoryRequest, response *category.UpdateCategoryResponse) error {
	cate := &model.Category{
		CategoryName:        request.CategoryName,
		CategoryLevel:       request.CategoryLevel,
		CategoryParent:      request.CategoryParent,
		CategoryImage:       request.CategoryImage,
		CategoryDescription: request.CategoryDescription,
	}
	err := c.CategoryService.UpdateCategory(cate)
	if err != nil {
		response.Message = "更新类型失败!"
		return err
	}
	response.Message = "更新类型成功!"
	return nil
}

// 删除类型
func (c *CategoryHandler) DeleteCategory(ctx context.Context, request *category.DeleteCategoryRequest, response *category.DeleteCategoryResponse) error {
	err := c.CategoryService.DeleteCategory(request.CategoryId)
	if err != nil {
		response.Message = "删除分类失败!"
		return err
	}
	response.Message = "删除分类成功!"
	return nil
}

// 查找分类
func (c *CategoryHandler) FindCategoryByName(ctx context.Context, request *category.FindByNameRequest, response *category.CategoryResponse) error {
	cate, err := c.CategoryService.FindCategoryByName(request.CategoryName)
	if err != nil {
		return err
	}
	//response.Id = cate.ID
	//response.CategoryName = cate.CategoryName
	//response.CategoryLevel = cate.CategoryLevel
	//response.CategoryParent = cate.CategoryParent
	//response.CategoryImage = cate.CategoryImage
	//response.CategoryDescription = cate.CategoryDescription
	//return nil
	return common.SwapTo(cate, response)
}

// 根据id查找
func (c *CategoryHandler) FindCategoryByID(ctx context.Context, request *category.FindByIDRequest, response *category.CategoryResponse) error {
	cate, err := c.CategoryService.FindCategoryByID(request.Id)
	if err != nil {
		return err
	}
	//response.Id = cate.ID
	//response.CategoryName = cate.CategoryName
	//response.CategoryLevel = cate.CategoryLevel
	//response.CategoryParent = cate.CategoryParent
	//response.CategoryImage = cate.CategoryImage
	//response.CategoryDescription = cate.CategoryDescription
	//return nil
	return common.SwapTo(cate, response)
}

// 根据级别查找
func (c *CategoryHandler) FindCategoriesByLevel(ctx context.Context, request *category.FindByLevelRequest, response *category.FindAllResponse) error {
	categories, err := c.CategoryService.FindCategoryByLevel(request.Level)
	if err != nil {
		return err
	}
	cates := make([]*category.CategoryResponse, len(categories))
	for _, cate := range categories {
		cateResponse := category.CategoryResponse{
			Id:                  cate.ID,
			CategoryName:        cate.CategoryName,
			CategoryLevel:       cate.CategoryLevel,
			CategoryParent:      cate.CategoryParent,
			CategoryImage:       cate.CategoryImage,
			CategoryDescription: cate.CategoryDescription,
		}
		cates = append(cates, &cateResponse)
	}
	response.Categories = cates
	return nil
}

// 根据parentId查找
func (c *CategoryHandler) FindCategoriesByParent(ctx context.Context, request *category.FindByParentRequest, response *category.FindAllResponse) error {
	categories, err := c.CategoryService.FindCategoryByParent(request.ParentId)
	if err != nil {
		return err
	}
	cates := make([]*category.CategoryResponse, len(categories))
	for _, cate := range categories {
		cateResponse := category.CategoryResponse{
			Id:                  cate.ID,
			CategoryName:        cate.CategoryName,
			CategoryLevel:       cate.CategoryLevel,
			CategoryParent:      cate.CategoryParent,
			CategoryImage:       cate.CategoryImage,
			CategoryDescription: cate.CategoryDescription,
		}
		cates = append(cates, &cateResponse)
	}
	response.Categories = cates
	return nil
}

// 查找所有的
func (c *CategoryHandler) FindAllCategories(ctx context.Context, request *category.FindAllRequest, response *category.FindAllResponse) error {
	categories, err := c.CategoryService.FindAllCategories()
	if err != nil {
		return err
	}
	cates := make([]*category.CategoryResponse, len(categories))
	for _, cate := range categories {
		cateResponse := category.CategoryResponse{
			Id:                  cate.ID,
			CategoryName:        cate.CategoryName,
			CategoryLevel:       cate.CategoryLevel,
			CategoryParent:      cate.CategoryParent,
			CategoryImage:       cate.CategoryImage,
			CategoryDescription: cate.CategoryDescription,
		}
		cates = append(cates, &cateResponse)
	}
	response.Categories = cates
	return nil
}
