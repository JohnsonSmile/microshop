// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/19 11:02
// 文件名称 ：   swap.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package common

import "encoding/json"

func SwapTo(src, dst interface{}) (err error) {
	bytes, err := json.Marshal(src)
	if err != nil {
		return err
	}
	return json.Unmarshal(bytes, dst)
}
