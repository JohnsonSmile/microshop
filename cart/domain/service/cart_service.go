// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/20 21:16
// 文件名称 ：   cart_service.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package service

import (
	"gitee.com/Johnsonsmile/microshop/cart/domain/model"
	"gitee.com/Johnsonsmile/microshop/cart/domain/repository"
)

type ICartDataService interface {
	AddCart(cart *model.Cart) (id int64, err error)
	DeleteCart(cid int64) (err error)
	UpdateCart(cart *model.Cart) (err error)
	FindCartByID(cid int64) (cart *model.Cart, err error)
	FindAllCart(uid int64) (carts []model.Cart, err error)
	CleanCart(uid int64) (err error)
	IncrNum(cid int64, num int64) (err error)
	DecrNum(cid int64, num int64) (err error)
}

// 创建cart数据服务
func NewCartDataService(repo repository.ICartRepository) ICartDataService {
	return &CartDataService{repo: repo}
}

type CartDataService struct {
	repo repository.ICartRepository
}

func (c *CartDataService) AddCart(cart *model.Cart) (id int64, err error) {
	return c.repo.CreateCart(cart)
}

func (c *CartDataService) DeleteCart(cid int64) (err error) {
	return c.repo.DeleteCartByID(cid)
}

func (c *CartDataService) UpdateCart(cart *model.Cart) (err error) {
	return c.repo.UpdateCart(cart)
}

func (c *CartDataService) FindCartByID(cid int64) (cart *model.Cart, err error) {
	return c.repo.FindCartByID(cid)
}

func (c *CartDataService) FindAllCart(uid int64) (carts []model.Cart, err error) {
	return c.repo.FindAll(uid)
}

func (c *CartDataService) CleanCart(uid int64) (err error) {
	return c.repo.CleanCart(uid)
}

func (c *CartDataService) IncrNum(cid int64, num int64) (err error) {
	return c.repo.IncrNum(cid, num)
}

func (c *CartDataService) DecrNum(cid int64, num int64) (err error) {
	return c.repo.DecrNum(cid, num)
}
