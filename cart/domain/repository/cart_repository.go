// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/20 20:54
// 文件名称 ：   cart_repository.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package repository

import (
	"errors"
	"gitee.com/Johnsonsmile/microshop/cart/domain/model"
	"github.com/jinzhu/gorm"
)

type ICartRepository interface {
	InitTable() error
	FindCartByID(id int64) (cart *model.Cart, err error)
	CreateCart(cart *model.Cart) (id int64, err error)
	DeleteCartByID(id int64) (err error)
	UpdateCart(cart *model.Cart) (err error)
	FindAll(uid int64) (carts []model.Cart, err error)
	CleanCart(uid int64) (err error)
	IncrNum(id int64, num int64) (err error)
	DecrNum(id int64, num int64) (err error)
}

// 创建repository
func NewCartRepository(db *gorm.DB) ICartRepository {
	return &CartRepository{db: db}
}

type CartRepository struct {
	db *gorm.DB
}

// 创建表
func (c *CartRepository) InitTable() error {
	return c.db.CreateTable(&model.Cart{}).Error
}

// 根据购物车id查询购物车信息
func (c *CartRepository) FindCartByID(id int64) (cart *model.Cart, err error) {
	cart = &model.Cart{}
	return cart, c.db.First(cart, id).Error
}

// 创建购物车
func (c *CartRepository) CreateCart(cart *model.Cart) (id int64, err error) {
	db := c.db.FirstOrCreate(cart, model.Cart{
		ProductID: cart.ProductID,
		SizeID:    cart.SizeID,
		UserID:    cart.UserID,
	})
	if db.Error != nil {
		return 0, err
	}
	if db.RowsAffected == 0 {
		return 0, errors.New("购物车插入失败!")
	}
	return cart.ID, nil
}

// 删除购物车信息
func (c *CartRepository) DeleteCartByID(id int64) (err error) {
	return c.db.Where("id = ?", id).Delete(&model.Cart{}).Error
}

// 更新cart
func (c *CartRepository) UpdateCart(cart *model.Cart) (err error) {
	return c.db.Model(cart).Update(cart).Error
}

// 获取结果集
func (c *CartRepository) FindAll(uid int64) (carts []model.Cart, err error) {
	return carts, c.db.Where("user_id = ?", uid).Find(carts).Error
}

// 清除购物车
func (c *CartRepository) CleanCart(uid int64) (err error) {
	return c.db.Where("user_id = ?", uid).Delete(&model.Cart{}).Error
}

// 添加商品数量
func (c *CartRepository) IncrNum(id int64, num int64) (err error) {
	cart := &model.Cart{ID: id}
	return c.db.Model(cart).UpdateColumn("num", gorm.Expr("num + ?", num)).Error
}

// 减少商品
func (c *CartRepository) DecrNum(id int64, num int64) (err error) {
	cart := &model.Cart{ID: id}
	db := c.db.Model(cart).Where("num >= ?", num).UpdateColumn("num", gorm.Expr("num - ?", num))
	if db.Error != nil {
		return db.Error
	}
	if db.RowsAffected == 0 {
		return errors.New("减少失败!")
	}
	return nil
}
