// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/20 20:52
// 文件名称 ：   cart.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package model

type Cart struct {
	ID        int64 `gorm:"primary_key;not_null;auto_increment" json:"id"`
	ProductID int64 `gorm:"not_null" json:"product_id"`
	Num       int64 `gorm:"not_null" json:"num"`
	SizeID    int64 `gorm:"not_null" json:"size_id"`
	UserID    int64 `gorm:"not_null" json:"user_id"`
}
