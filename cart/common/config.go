// Copyright 2021 by 马万里. All rights reserved.
// 开发团队 ：   鸡中之霸
// 开发人员 ：   马万里
// 开发时间 ：   2021/3/19 11:11
// 文件名称 ：   config.go
// 工程名称 ：   microshop
// 开发工具 ：   GoLand
//

package common

import (
	"fmt"
	"github.com/micro/go-micro/v2/config"
	"github.com/micro/go-plugins/config/source/consul/v2"
)

// 获取consul的配置中心
func GetConsulConfig(host string, port int64, prefix string) (conf config.Config, err error) {
	// 创建配置中心源
	source := consul.NewSource(
		// 设置地址
		consul.WithAddress(fmt.Sprintf("%s:%d", host, port)),
		// 设置前缀,默认为:micro/config
		consul.WithPrefix(prefix),
		// 是否移除前缀, true可以不带前缀
		consul.StripPrefix(true),
	)
	// 新建配置中心
	conf, err = config.NewConfig()
	if err != nil {
		return nil, err
	}
	// 加载配置中心信息
	err = conf.Load(source)
	if err != nil {
		return nil, err
	}
	return conf, nil
}
