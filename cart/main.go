package main

import (
	"fmt"
	"gitee.com/Johnsonsmile/microshop/cart/common"
	"gitee.com/Johnsonsmile/microshop/cart/domain/repository"
	"gitee.com/Johnsonsmile/microshop/cart/domain/service"
	"gitee.com/Johnsonsmile/microshop/cart/handler"
	cart "gitee.com/Johnsonsmile/microshop/cart/proto"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-plugins/registry/consul/v2"
	"github.com/micro/go-plugins/wrapper/ratelimiter/uber/v2"
	opentracing2 "github.com/micro/go-plugins/wrapper/trace/opentracing/v2"
	"github.com/opentracing/opentracing-go"
)

// 限流,后期卸载配置中心
var QPS = 100

func main() {

	// 配置中心
	conf, err := common.GetConsulConfig("47.112.13.65", 8500, "/micro/config")
	if err != nil {
		logger.Error(err)
	}
	// 注册中心
	reg := consul.NewRegistry(
		registry.Addrs("47.112.13.65:8500"),
	)

	// 链路追踪
	tracer, closer, err := common.NewTracer("go.micro.service.cart", "47.112.13.65:6381")
	if err != nil {
		logger.Error(err)
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	// Create service
	srv := micro.NewService(
		micro.Name("go.micro.service.cart"),
		micro.Version("latest"),
		micro.Registry(reg),
		micro.WrapHandler(opentracing2.NewHandlerWrapper(opentracing.GlobalTracer())),
		micro.WrapHandler(ratelimit.NewHandlerWrapper(QPS)),
	)
	srv.Init()

	// 创建数据库
	mysqlConf, err := common.GetMysqlConfigFromConsul(conf, "mysql")
	if err != nil {
		logger.Fatal(err)
	}
	mysqlArgs := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=true&loc=Local",
		mysqlConf.User,
		mysqlConf.Pwd,
		mysqlConf.Host,
		mysqlConf.Port,
		mysqlConf.Database,
	)
	db, err := gorm.Open("mysql", mysqlArgs)
	if err != nil {
		logger.Error(err)
	}
	defer db.Close()
	db.SingularTable(true)
	// 创建repository
	cartRepository := repository.NewCartRepository(db)
	//err = cartRepository.InitTable()
	//if err != nil {
	//	logger.Error(err)
	//}
	// 创建dataservice
	dataService := service.NewCartDataService(cartRepository)

	// Register handler
	err = cart.RegisterCartHandler(srv.Server(), &handler.CartHandler{CartDataService: dataService})
	if err != nil {
		logger.Error(err)
	}

	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
